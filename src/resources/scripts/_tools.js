import $ from 'jquery';
import svg4everybody from 'svg4everybody';

export class Tools {
  static initSvgPolyfill() {
    svg4everybody();
  }

  static isTouch() {
    const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');

    const mq = (query) => {
      return window.matchMedia(query).matches;
    };

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }

    const query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');

    return mq(query);
  }
}
