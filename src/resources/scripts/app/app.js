import $ from 'jquery';
import { SELF_STATE_ID } from './constants';
import { Tools } from "../_tools";

export default class App {
  static get selectors() {
    return {
      main: '.b-pack-plates',
      pack: '.b-pack',
      actionLink: '.b-pack__action-link',
      packPlate: '.b-pack__plate-wrap'
    };
  }
  static get stateClasses() {
    return [
      {
        selfStateID: SELF_STATE_ID.SELECTED,
        clas: 'b-pack_selected'
      },
      {
        selfStateID: SELF_STATE_ID.DISABLED,
        clas: 'b-pack_disabled'
      },
      {
        selfStateID: SELF_STATE_ID.SELECTED_HOVER,
        clas: 'b-pack_selected-hover'
      },
    ];
  }
  static init($context = $(document), options = {}) {
    $(App.selectors.main).each((index, block) => {
      const $block = $(block);
      if($block.data('block-api')) return $block.data('block-api');
      return new App($block, options);
    });
  }
  constructor($block, options) {
    this.params = $.extend({
      selectors: App.selectors,
      classes: App.stateClasses
    }, options);

    this.elems = {
      $main: $block,
      $packs: $block.find(this.params.selectors.pack),
      $actionLinks: $block.find(this.params.selectors.actionLink),
      $packPlates: $block.find(this.params.selectors.packPlate)
    };

    this.state = {
      packs: [],
    };

    this.initBlockApi();
    this.initPacks();
    this.renderPacks();
    this.handleEvents();
  }
  initBlockApi() {
    this.elems.$main.data('block-api', this);
  }
  updateState(newState) {
    this.state = {
      ...this.state,
      ...newState
    };
  }
  initPacks() {
    const packs = [];
    this.elems.$packs.each((index, pack) => {
      const $pack = $(pack);
      $pack.data('pack-id', index);
      const inlineConfig = $pack.data('config') || {};
      packs.push({
        id: index,
        $link: $(pack),
        selfStateID: SELF_STATE_ID.DEFAULT,
        ...inlineConfig
      });
    });

    this.updateState({ packs });
  }
  renderPacks() {
    this.state.packs.forEach((pack, index) => {
      const $pack = pack.$link;
      this.renderPackByStateId($pack, pack.selfStateID);
    });
  }
  renderPackByStateId($pack, selfStateID) {
    const classes = this.params.classes;
    const _removeAllClasses = ($elem) => {
      for (const clasItem of classes) {
        $elem.removeClass(clasItem.clas);
      }
    };
    const _addClassByStateId = ($elem, stateId) => {
      const classElem = classes.find((element) => element.selfStateID === stateId);
      $elem.addClass(classElem.clas);
    };
    _removeAllClasses($pack);
    if (selfStateID !== SELF_STATE_ID.DEFAULT) {
      _addClassByStateId($pack, selfStateID);
    }
  }
  getPackStateById(id) {
    return this.state.packs.find((pack) => {
      return pack.id === id;
    });
  }
  getNewPacksWithStateId(packId, newSelfState) {
    return this.state.packs.map((currPack) => {
      if (currPack.id === packId) {
        return {
          ...currPack,
          selfStateID: newSelfState
        };
      } else {
        return { ...currPack };
      }
    });
  }
  togglePackState = (packId, toggler) => {
    let newStateId = null;
    switch (toggler) {
      case 'disactivate':
        newStateId = SELF_STATE_ID.DEFAULT;
        break;
      case 'select':
        newStateId = SELF_STATE_ID.SELECTED;
        break;
      case 'select-hover':
        newStateId = SELF_STATE_ID.SELECTED_HOVER;
        break;
    }

    const newPacks = this.getNewPacksWithStateId(packId, newStateId);

    this.updateState({ packs: newPacks });
    this.renderPacks();
  };
  handleClick($pack) {
    const packId = $pack.data('pack-id');
    const pack = this.getPackStateById(packId);

    switch (pack.selfStateID) {
      case SELF_STATE_ID.DISABLED:
        break;
      case SELF_STATE_ID.SELECTED:
        this.togglePackState(packId, 'disactivate');
        break;
      case SELF_STATE_ID.SELECTED_HOVER:
        this.togglePackState(packId, 'disactivate');
        break;
      default:
        this.togglePackState(packId, 'select-hover');
    }
  }
  handleMouseLeave($pack) {
    const packId = $pack.data('pack-id');
    const pack = this.getPackStateById(packId);

    let newPacks = this.state.packs;
    if (pack.selfStateID === SELF_STATE_ID.SELECTED_HOVER) {
      newPacks = this.getNewPacksWithStateId(packId, SELF_STATE_ID.SELECTED);
    }

    this.updateState({ packs: newPacks });
    this.renderPacks();
  }
  handleEvents() {
    if (!Tools.isTouch()) {
      this.elems.$packPlates.on('click', (event) => {
        const $pack = $(event.currentTarget).closest(this.elems.$packs);
        this.handleClick($pack);
      });
      this.elems.$actionLinks.on('click', (event) => {
        event.preventDefault();
        const $pack = $(event.currentTarget).closest(this.elems.$packs);
        this.handleClick($pack);
        this.handleMouseLeave($pack);
      });
      this.elems.$packPlates.on('mouseleave', (event) => {
        const $pack = $(event.currentTarget).closest(this.elems.$packs);
        this.handleMouseLeave($pack);
      });
    } else {
      this.elems.$packPlates.on('click', (event) => {
        const $pack = $(event.currentTarget).closest(this.elems.$packs);
        this.handleClick($pack);
        this.handleMouseLeave($pack);
      });
      this.elems.$actionLinks.on('click', (event) => {
        event.preventDefault();
        const $pack = $(event.currentTarget).closest(this.elems.$packs);
        this.handleClick($pack);
        this.handleMouseLeave($pack);
      });
    }
  }
}
