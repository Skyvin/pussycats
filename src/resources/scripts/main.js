import "@babel/polyfill";
import $ from 'jquery';
import { Tools } from '../scripts/_tools';
import App from './app/app';

Tools.initSvgPolyfill();

$(() => {
  App.init();
});
